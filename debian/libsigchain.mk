NAME = libsigchain

SOURCES = sigchain.cc
SOURCES := $(foreach source, $(SOURCES), sigchainlib/$(source))

CPPFLAGS += \
  -Isigchainlib \

LDFLAGS += \
  -shared \
  -Wl,-soname,$(NAME).so.0

debian/out/$(NAME).so.0: $(SOURCES)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)
	cd debian/out && ln -s $(NAME).so.0 $(NAME).so
