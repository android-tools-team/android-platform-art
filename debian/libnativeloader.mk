NAME = libnativeloader

SOURCES = libnativeloader/native_loader.cpp
OBJECTS = $(SOURCES:.cpp=.o)

CPPFLAGS += \
  -I/usr/include/android/nativehelper \
  -Ilibnativebridge/include \
  -Ilibnativeloader/include \

LDFLAGS += \
  -L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -Ldebian/out \
  -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -ldl \
  -lnativebridge \
  -shared

debian/out/$(NAME).so.0: $(OBJECTS)
	$(CXX) -o $@ $^ $(LDFLAGS)
	cd debian/out && ln -s $(NAME).so.0 $(NAME).so

$(OBJECTS): %.o: %.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS) $(CPPFLAGS)
