NAME = libdexfile_support

SOURCES = libdexfile/external/dex_file_supp.cc

CPPFLAGS += \
  -DNO_DEXFILE_SUPPORT \
  -Ilibartbase \
  -Ilibdexfile \
  -Ilibdexfile/external/include \

LDFLAGS += \
  -shared \
  -Wl,-soname,$(NAME).so.0

debian/out/$(NAME).so.0: $(SOURCES)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS)
	cd debian/out && ln -s $(NAME).so.0 $(NAME).so
