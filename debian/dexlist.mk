NAME = dexlist

SOURCES = dexlist.cc
SOURCES := $(foreach source, $(SOURCES), dexlist/$(source))
OBJECTS = $(SOURCES:.cc=.o)

CPPFLAGS += \
  -I/usr/include/android/nativehelper \
  -Ilibartbase \
  -Ilibdexfile \
  -Iruntime \

# See the comment in `dexdump.mk`
LDFLAGS += -nodefaultlibs \
  -L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -Ldebian/out \
  -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -lsigchain \
  -lart \
  -latomic \
  -lbacktrace \
  -lbase \
  -lc \
  -lcpu_features \
  -ldexfile_support \
  -ldl \
  -lgcc_s \
  -llog \
  -llz4 \
  -lnativebridge \
  -lnativeloader \
  -lpthread \
  -lstdc++ \
  -ltinyxml2 \
  -lz \
  -lziparchive \

debian/out/$(NAME): $(OBJECTS)
	$(CXX) -o $@ $^ $(LDFLAGS)

$(OBJECTS): %.o: %.cc
	$(CXX) -c -o $@ $< $(CPPFLAGS) $(CXXFLAGS)
