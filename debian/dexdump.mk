NAME = dexdump

SOURCES = \
  dexdump.cc \
  dexdump_cfg.cc \
  dexdump_main.cc \

SOURCES := $(foreach source, $(SOURCES), dexdump/$(source))
OBJECTS = $(SOURCES:.cc=.o)

CPPFLAGS += \
  -I/usr/include/android/nativehelper \
  -Idexdump \
  -Ilibartbase \
  -Ilibdexfile \
  -Ilibdexfile/external/include \
  -Iruntime \

# libsigchain defines wrapper functions around sigaction() family. In order to
# override the ones provided by libc, libsignal must appear in the shared
# object dependency tree before libc in the breadth-first order.
LDFLAGS += -nodefaultlibs \
  -L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -Ldebian/out \
  -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
  -lsigchain \
  -lart \
  -latomic \
  -lbacktrace \
  -lbase \
  -lc \
  -lcpu_features \
  -ldexfile_external \
  -ldexfile_support \
  -ldl \
  -lgcc_s \
  -llog \
  -llz4 \
  -lnativebridge \
  -lnativeloader \
  -lpthread \
  -lstdc++ \
  -ltinyxml2 \
  -lz \
  -lziparchive \

debian/out/$(NAME): $(OBJECTS)
	$(CXX) -o $@ $^ $(LDFLAGS)

$(OBJECTS): %.o: %.cc
	$(CXX) -c -o $@ $< $(CPPFLAGS) $(CXXFLAGS)
